# LibreCube Board Template

The building block for all kinds of electrical systems for is the PC/104-alike
printed circuit board. This repository holds the electrical and mechanical
template of such board to use for your project.

Find the full board specification here:
https://wiki.librecube.org/index.php?title=LibreCube_Board_Specification

![](docs/pcb_template_top.jpg)
