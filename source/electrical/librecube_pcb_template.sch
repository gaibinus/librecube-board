EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	8800 1050 9200 1050
Wire Wire Line
	8800 2550 9200 2550
Wire Wire Line
	8800 1150 9200 1150
Wire Wire Line
	10100 2550 9700 2550
Wire Wire Line
	8800 4000 9200 4000
Wire Wire Line
	9200 3900 8800 3900
Text Label 8800 1050 0    50   ~ 0
CAN_A_L
Text Label 8800 2550 0    50   ~ 0
CHARGE
Text Label 8800 3900 0    50   ~ 0
CAN_B_L
Text Label 8800 4000 0    50   ~ 0
CAN_B_H
Text Label 8800 1150 0    50   ~ 0
CAN_A_H
Text Label 10100 2550 2    50   ~ 0
CHARGE
$Comp
L Connector_Generic:Conn_02x26_Odd_Even J1
U 1 1 5C4C195F
P 9400 2250
F 0 "J1" H 9450 3667 50  0000 C CNN
F 1 "ESQ-126-39-x-D" H 9450 3576 50  0000 C CNN
F 2 "librecube_pcb_template:ESQ-126-39-x-D" H 9400 2250 50  0001 C CNN
F 3 "https://www.mouser.sk/datasheet/2/527/esq_th-1369982.pdf" H 9400 2250 50  0001 C CNN
	1    9400 2250
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x26_Odd_Even J2
U 1 1 5C2F526D
P 9400 5100
F 0 "J2" H 9450 6517 50  0000 C CNN
F 1 "ESQ-126-39-x-D" H 9450 6426 50  0000 C CNN
F 2 "librecube_pcb_template:ESQ-126-39-x-D" H 9400 5100 50  0001 C CNN
F 3 "https://www.mouser.sk/datasheet/2/527/esq_th-1369982.pdf" H 9400 5100 50  0001 C CNN
	1    9400 5100
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H4
U 1 1 5E50DEC4
P 1000 7400
F 0 "H4" H 1100 7449 50  0000 L CNN
F 1 "MountingHole_Pad" H 1100 7358 50  0000 L CNN
F 2 "librecube_pcb_template:MountingHole_3.2mm_M3_PC104" H 1000 7400 50  0001 C CNN
F 3 "~" H 1000 7400 50  0001 C CNN
	1    1000 7400
	1    0    0    -1  
$EndComp
NoConn ~ 1000 6500
NoConn ~ 1000 6850
NoConn ~ 1000 7150
NoConn ~ 1000 7500
$Comp
L Mechanical:MountingHole_Pad H3
U 1 1 5E50E2B0
P 1000 7050
F 0 "H3" H 1100 7099 50  0000 L CNN
F 1 "MountingHole_Pad" H 1100 7008 50  0000 L CNN
F 2 "librecube_pcb_template:MountingHole_3.2mm_M3_PC104" H 1000 7050 50  0001 C CNN
F 3 "~" H 1000 7050 50  0001 C CNN
	1    1000 7050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H2
U 1 1 5E50E4A0
P 1000 6750
F 0 "H2" H 1100 6799 50  0000 L CNN
F 1 "MountingHole_Pad" H 1100 6708 50  0000 L CNN
F 2 "librecube_pcb_template:MountingHole_3.2mm_M3_PC104" H 1000 6750 50  0001 C CNN
F 3 "~" H 1000 6750 50  0001 C CNN
	1    1000 6750
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H1
U 1 1 5E50E62D
P 1000 6400
F 0 "H1" H 1100 6449 50  0000 L CNN
F 1 "MountingHole_Pad" H 1100 6358 50  0000 L CNN
F 2 "librecube_pcb_template:MountingHole_3.2mm_M3_PC104" H 1000 6400 50  0001 C CNN
F 3 "~" H 1000 6400 50  0001 C CNN
	1    1000 6400
	1    0    0    -1  
$EndComp
Text Label 10100 5100 2    50   ~ 0
5V
Text Label 8800 5100 0    50   ~ 0
5V
Wire Wire Line
	8800 5100 9200 5100
Wire Wire Line
	8800 5300 9200 5300
Wire Wire Line
	8800 5400 9200 5400
Wire Wire Line
	9700 5300 10100 5300
Wire Wire Line
	9700 5400 10100 5400
Text Label 8800 5300 0    50   ~ 0
GND
Text Label 8800 5400 0    50   ~ 0
GND
Text Label 10100 5300 2    50   ~ 0
GND
Text Label 10100 5400 2    50   ~ 0
GND
Wire Wire Line
	10100 5100 9700 5100
Wire Wire Line
	8800 6100 9200 6100
Wire Wire Line
	10100 6100 9700 6100
Text Label 8800 6100 0    50   ~ 0
VBAT
Text Label 10100 6100 2    50   ~ 0
VBAT
Wire Wire Line
	8800 2650 9200 2650
Wire Wire Line
	10100 2650 9700 2650
Text Label 8800 2650 0    50   ~ 0
GND
Text Label 10100 2650 2    50   ~ 0
GND
$EndSCHEMATC
